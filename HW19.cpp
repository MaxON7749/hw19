#include <iostream>
#include <string>

using namespace std;

class Animal
{
public:
	Animal() {}
	virtual void Voice()
	{
		cout << "Animal Voice\n";
	}
};

class Dog : public Animal
{
public:
	Dog() {}
	void Voice() override
	{
		cout << "Woof\n";
	}
};

class Cat :public Animal {
public:
	Cat() {}
	void Voice() override {
		cout << "Meow\n";
	}
};

class Cow :public Animal
{
public:
	Cow() {}
	void Voice() override {
		cout << "Mooo\n";
	}
};

class Lion : public Animal
{
public:
	Lion() {}
	void Voice() override {
		cout << "Roar\n";
	}
};

int main()
{
	Animal* array[4];
	Animal an;
	array[0] = new Dog();
	array[1] = new Cat();
	array[2] = new Cow();
	array[3] = new Lion();
	for (Animal* element : array) {
		element->Voice();
	}
}

